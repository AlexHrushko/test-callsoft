import React, { Component } from 'react';
import './App.css';
import { TYPE_PARSEL } from "./types.js"

import Country from "./components/Country";
import Item from "./components/Item"

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      fromAddressInformation: {
        country: 'None',
        address: '',
      },
      toAddressInformation: {
        country: 'None',
        address: ''
      },
      positions: [
        this.createNewPosition()
      ]
    };

    this.onChangeFromInformation = this.onChangeFromInformation.bind(this);
    this.onChangeToInformation = this.onChangeToInformation.bind(this);
    this.onChange = this.onChange.bind(this);
    this.addPosition = this.addPosition.bind(this);
    this.deletePosition = this.deletePosition.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.createNewPosition = this.createNewPosition.bind(this);
  }

  createNewPosition() {
    return {
      type: TYPE_PARSEL,
      information: {
      }
    }
  };

  onChangeFromInformation(data) {
    this.setState({
      fromAddressInformation: data
    })
  }

  onChangeToInformation(data) {
    this.setState({
      toAddressInformation: data
    })
  }

  onChange(i, data) {

    this.setState({
      positions:
      this.state.positions.map((position, index) => {

        if (index === i) {
          return data;
        };
        return position;
      })
    })
  }

  addPosition() {
    let newPosition = this.createNewPosition();
    this.setState({
      positions: [
        ...this.state.positions, newPosition
      ]
    })
  }

  deletePosition() {
    this.setState({
      positions:
      this.state.positions.slice(0, -1)
    })
  }

  handleSubmit(event) {
    event.preventDefault();

    console.log(this.state);
  }

  render() {

    return (
      <div className="App container">
        <form onSubmit={this.handleSubmit}>
          <div className="panel panel-default panel-body-otstup">
            <div className="panel-body">
              <div className="form-container">
                <div className="row">
                  <div className="col-lg-6">
                    <Country
                      data={this.state.fromAddressInformation}
                      onChange={this.onChangeFromInformation} />
                  </div>
                  <div className="col-lg-6">
                    <Country
                      data={this.state.toAddressInformation}
                      onChange={this.onChangeToInformation} />
                  </div>
                </div>
                <div style={{ marginTop: 15 }}>
                  <Item
                    key="0"
                    data={this.state.positions[0]}
                    onChange={this.onChange.bind(this, 0)} />
                </div>
              </div>
            </div>
          </div>
          {this.state.positions.slice(1).map((position, index) =>
            <div className="panel panel-default panel-body-otstup" key={index + 1}>
              <div className="panel-body">
                <div className="form-container">
                  <Item
                    data={position}
                    onChange={this.onChange.bind(this, index + 1)} />
                </div>
              </div>
            </div>
          )}
          <div className="row">
            <div className="col-md-4">
              <button type="button"
                className="btn btn-primary color-button-1"
                onClick={this.deletePosition}
                disabled={this.state.positions.length === 1}>
                <strong>- Remove</strong>
              </button>
              <button type="button"
                className="btn btn-primary color-button-1 button-otstup"
                onClick={this.addPosition}>
                <strong>+ Add parcel or pallet</strong>
              </button>
            </div>
            <div className="col-md-2 col-md-offset-5">
              <button className="btn btn-primary color-button-2"
                type="submit"
                value="Submit">
                <strong>Quote & Book</strong>
              </button>
            </div>
          </div>
        </form>
      </div >
    );
  }
}

export default (App);

