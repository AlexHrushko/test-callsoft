//@flow

import React, { Component } from 'react';

class PalletInformation extends Component {
    constructor(props) {
        super(props);

        this.state = this.props.data;

    }
    onChange = (event) => {
        const newData = {
            ...this.state,
            [event.target.name]: event.target.value
        };

        this.setState(newData, () => {
            this.props.onChange(newData);
        });
    }

    componentDidMount() {
        this.props.onChange(this.props.data);
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-3">
                    <label htmlFor="pallet_number">Number of pallets</label>
                    <div className="input-group">
                        <input
                            type="number"
                            name="pallet_number"
                            className="form-control"
                            min={0}
                            defaultValue={this.props.data.pallet_number}
                            onChange={this.onChange} />
                        <div className="input-group-addon">Pcs</div>
                    </div>
                </div>
                <div className="col-md-3">
                    <label htmlFor="pallet_height">Height</label>
                    <div className="input-group">
                        <input
                            type="number"
                            name="pallet_height"
                            className="form-control"
                            min="0"
                            defaultValue={this.props.data.pallet_height}
                            onChange={this.onChange} />
                        <div className="input-group-addon">Cm</div>
                    </div>
                </div>
                <div className="col-md-3">
                    <label htmlFor="pallet_weight">Weight for pallet</label>
                    <div className="input-group">
                        <input
                            type="number"
                            name="pallet_weight"
                            className="form-control"
                            min="0"
                            defaultValue={this.props.data.pallet_weight}
                            onChange={this.onChange} />
                        <div className="input-group-addon">Kg</div>
                    </div>
                </div>
            </div>
        )
    }
}


PalletInformation.defaultProps = {
    data: {
        pallet_number: "0",
        pallet_height: "0",
        pallet_weight: "0"
    }
}

export default (PalletInformation);