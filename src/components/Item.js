//@flow
import React, { Component } from 'react';
import { TYPE_PARSEL, TYPE_PALLET } from "../types.js";
import PalletInformation from "./PalletInformation";
import ParcelInformation from "./ParcelInformation";

let jQuery = require('jquery');

class Item extends Component {

    onChangeType = () => {

        const newData = {
            ...this.props.data,
            type: (this.props.data.type === TYPE_PARSEL ? TYPE_PALLET : TYPE_PARSEL)
        };
        this.props.onChange(newData);
    }

    onChange = (infoData) => {

        const newData = {
            ...this.props.data,
            information: infoData
        };
        this.props.onChange(newData);
    }

    componentDidMount() {
        jQuery(this.switcher).bootstrapSwitch();
        jQuery(this.switcher).on('switchChange.bootstrapSwitch', (event, state) => {
            this.onChangeType()
        });
    }
    render() {
        return (
            <div className="row">
                <div className="col-md-2" style={{ marginTop: 24 }}>
                    <input type="checkbox"
                        name="my-checkbox"
                        ref={input => this.switcher = input}
                        checked
                        onChange={()=>{}}
                        data-on-text={TYPE_PARSEL}
                        data-off-text={TYPE_PALLET}
                        data-off-color="primary"/>
                </div>
                <div className="col-md-10">
                    {this.props.data.type === TYPE_PARSEL ? <ParcelInformation onChange={this.onChange} /> : <PalletInformation onChange={this.onChange} />}
                </div>
            </div>
        )
    }
}

export default (Item);