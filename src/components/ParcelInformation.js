//@flow
import React, { Component } from 'react';

class ParcelInformation extends Component {
    constructor(props) {
        super(props);
        this.state = this.props.data;
    }
    onChange = (event) => {
        const newData = {
            ...this.state,
            [event.target.name]: event.target.value
        };
        this.setState(newData, () => {
            this.props.onChange(newData);
        });
    }

    componentDidMount() {
        this.props.onChange(this.props.data);
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-3">
                    <label htmlFor="parcel_weight">Weight</label>
                    <div className="input-group">
                        <input
                            type="number"
                            name="parcel_weight"
                            className="form-control"
                            min={0}
                            label="Weight"
                            defaultValue={this.props.data.parcel_weight}
                            onChange={this.onChange} />
                        <div className="input-group-addon">Kg</div>
                    </div>
                </div>
                <div className="col-md-3">
                    <label htmlFor="parcel_length">Length</label>
                    <div className="input-group">
                        <input
                            type="number"
                            name="parcel_length"
                            className="form-control"
                            min="0"
                            defaultValue={this.props.data.parcel_length}
                            label="Length"
                            onChange={this.onChange} />
                        <div className="input-group-addon">Cm</div>
                    </div>
                </div>
                <div className="col-md-3">
                    <label htmlFor="parcel_width">Width</label>
                    <div className="input-group">
                        <input
                            type="number"
                            name="parcel_width"
                            className="form-control"
                            min="0"
                            defaultValue={this.props.data.parcel_width}
                            label="Width"
                            onChange={this.onChange} />
                        <div className="input-group-addon">Cm</div>
                    </div>
                </div>
                <div className="col-md-3">
                    <label htmlFor="parcel_height">Height</label>
                    <div className="input-group">
                        <input
                            type="number"
                            name="parcel_height"
                            className="form-control"
                            min="0"
                            label="Height"
                            defaultValue={this.props.data.parcel_height}
                            onChange={this.onChange} />
                        <div className="input-group-addon">Cm</div>
                    </div>
                </div>
            </div>
        )
    }
}

ParcelInformation.defaultProps = {
    data: {
        parcel_weight: "0",
        parcel_length: "0",
        parcel_width: "0",
        parcel_height: "0",
    }
}

export default (ParcelInformation);