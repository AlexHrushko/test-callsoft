//@flow

import React, { Component } from 'react';

const countries = [
    'Belgium',
    'Ukraine',
    'USA',
    'France',
    'England',
    'Kanada',
    'Brazil',
    'Italy',
    'Spain',
    'Poland',
]
class Country extends Component {

    state = {
        country: String,
        address: String
    }

    constructor(props) {
        super(props);

        this.state = {
            country: "None",
            address: ''
        }

    }

    onChangeAddress = (event) => {
        
        this.setState({
            ...this.state,
            address: event.target.value
        }, () => {
            this.props.onChange(this.state);
        });
    }

    onChangeCountry = (value) => {
        this.setState({
            ...this.state,
            country: value
        }, () => {
            this.props.onChange(this.state);
        });
    }

    render() {
        return (
            <div className="input-group">
                <div className="input-group-btn">
                    <button type="button"
                        className="btn btn-primary color-button-3 dropdown-toggle"
                        data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <strong>{this.state.country}</strong> <span className="caret"></span>
                    </button>
                    <ul className="dropdown-menu">
                        {countries.map(country => (
                            <li key={country}>
                                <a onClick={this.onChangeCountry.bind(this, country)} >
                                    {country}
                                </a>
                            </li>
                        ))}
                    </ul>
                </div>
                <input
                    type="text"
                    className="form-control"
                    name="fromAddress"
                    placeholder="ZIP Code, Address"
                    value={this.state.address}
                    onChange={this.onChangeAddress} />
            </div>
        )
    }
}


export default (Country);